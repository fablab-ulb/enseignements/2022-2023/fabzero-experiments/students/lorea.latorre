# 5. Dynamique de groupe et projet final

## 5.1 Cours 1 : Les arbres à problèmes et objectifs

Lors de ce cours sur l'analyse et la conception de projet, on a appris à utiliser une technique particulière qui s'appelle **l'arbre à problèmes et l'arbre à objectifs**. Cette [vidéo](https://youtu.be/9KIlK61RInY) en explique le principe en quelques minutes.  
  
Pour s'exercer, il nous a été demandé de choisir un des [projets du fablab](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/class-website/#archives) ou un des [projets de *frugal science*](https://gitlab.com/fablab-ulb/enseignements/fabzero/projects/-/blob/main/open-source-lab.md) et de créer un arbre à problèmes et de transformer celui-ci en arbre à objectifs par la suite.  
  
### 5.1.1 Les centrifugeuses
  
Pour cet exercice, j'ai choisi le projet "[*Paperfuge*](https://www.nature.com/articles/s41551-016-0009/)", le projet de la [centrifugeuse en papier](https://youtu.be/isMYGtCFljc).  
  
Les centrifugeuses sont extrèmement importantes en biologie car elles utilisent le principe de force centrifuge pour séparer des composés d'un mélange, par exemple séparer les globules rouges des globules blancs dans le sang. Grâce à cela, il est possible, par la suite, d'analyser le sang et de dépister des maladies.  
Le problème est que se sont des machines coûteuses, encombrantes et qui nécessitent d'être branchées à l'électricité. Certains pays pauvres en ressources et n'ayant pas un bon accès à l'électricité sont donc extrèmement désavantagés.  

### 5.1.2 Arbre à problèmes 

![](images/arbre-%C3%A0-probl%C3%A8mes1.jpg)  
  
### 5.1.3 Arbre à objectifs 
  
![](images/arbres-%C3%A0-solutions1.jpg)  
  
### 5.1.4 Résultats
  
La création d'une centrigugeuse faite de papier et d'un peu de ficelle a été une révolution énorme ! Ridiculement abordable à tous les pays, le prix d'une centrifugeuse en papier peut être de **quelques centimes**. En papier, la centrifugeuse ne prend pas beaucoup de place mais reste la centrifugeuse la plus rapide, elle atteint **125000 tr/min** ! En 15 minutes, la séparation est telle que l'analyse de l'échantillon permet de **dépister le paludisme**.  
   
## 5.2 Cours 2 : Formation des groupes et brainstorming des problématiques

Pour ce cours, il nous a été demandé d'amener un objet qui représente une problématique qui nous intéresse.  
  
J'ai choisi d'amener ma **carte bancaire** pour représenter la suppression des distributeurs de billets ! En effet, les banques cherchent à faire disparaitre l'argent liquide et selon moi, il s'agit d'un moyen de gagner plus de contrôle sur l'argent. De plus je trouve ça scandalisant que l'on soit en train de se diriger vers un monde dans lequel ce que l'on possède n'est plus **matériel** mais **numérique**.  
  
Si ce sujet vous intéresse et vous voulez en apprendre un peu plus, je vous renvoie à [cette courte vidéo](https://youtu.be/drEpDw4qP6A) ou encore [ce document](https://www.financite.be/sites/default/files/lettre_argumentaire.pdf) écrit par [Financité](https://www.financite.be/fr), un *mouvement pluraliste dont le but désintéressé est de développer la recherche, l’éducation et l’action en matière de finance responsable et solidaire afin de contribuer à une société plus juste et plus humaine*.  
  
![](images/financit%C3%A9.png)  
  
Sachez qu'il s'agit d'un sujet très actuel pour lequel une [pétition](https://www.financite.be/petitionbatopin) tourne **en ce moment même** donc n'hésitez pas à la signer ! Il s'agit de sauver un peu plus de 5000 distributeurs de billets en Belgique !
  
### 5.2.1 Les groupes

Le choix des groupes a été la toute **première** étape de ce cours, mais on ne les a pas choisis par hasard !  
Grâce aux objets amenés par chacun des élèves, il nous a été demandé de nous diriger vers des personnes qui pourraient avoir une problématique **ressemblante** à la nôtre. Au lieu d'utiliser ma carte bancaire, j'ai préféré continuer l'activité avec une pièce de 1 euro par précaution.   
Je dois avouer que ma problématique me paraissait totalement venir d'une autre planète par rapport aux objets des autres élèves. *Coquillage, peau de banane, clé de voiture, bouteille en plastique, microscope, ticket de train,...*  
Rien ne ressemblait à mon problème de disparition de l'argent liquide. J'ai donc eu du mal a trouver des étudiants vers qui me diriger.  
Après avoir discuté avec ma professeure, Chloé Crokart, qui m'a demandé de faire abstraction de la problématique et de simplement regarder les objets, j'ai finalement rejoint un groupe de deux filles et un garçon. Leurs objets seront révélés dans quelques instants, soyez patients !  
En effet, mon objet étant globalement "l'argent", je pouvais *matcher* avec plus ou moins n'importe quel étudiant.  
Plus tard, un dernier étudiant s'est joint à nous, dès lors, mon groupe était formé.  
  
Voilà 4 des 5 objets de notre groupe : 
  
![](images/objets-probl%C3%A9matiques1.png)
  
Le dernier est une clé de voiture !
  
### 5.2.2 Faire connaissance

Une fois notre groupe formé, [Ayman Taifour](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/aymane.taifour/), qui étudie l'informatique, [Mariam Mekray](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/mariam.mekray/) et [Virginie Louckx](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/virginie.louckx/), deux étudiantes en bioingénieurie, [Matthew Doyle](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/), élève en 3è année de biologie, et moi même sommes allés nous assoir autour d'une grande feuille et avons appris à nous connaitre.  
Je connaissais déjà Virginie, ma binôme de soutien ainsi que Matthew avec qui j'ai travaillé sur le module dédié aux imprimantes 3D. Mariam et Ayman m'étaient, eux, tout à fait étrangé mais je suis bien contente de les avoir dans mon groupe !

### 5.2.3 Choisir une thématique 

**1.** Pourquoi cet objet ?

Après avoir appris à nous connaitre et avoir rigolé quelques minutes, il nous a été demandé de chacun à notre tour, expliquer les raisons pour lesquelles nous avions choisi notre objet.  
Très brièvement, Ayman, Virginie et Mariam qui avaient respectivement amené un ticket de train, des clés de voiture et une voiture en jouet ont choisi ces objets pour représenter toute la pollution lié aux transports, de leur production à leur destruction. Matthew représentait, avec son emballage de yaourt au soja, la pollution engendrée par le transport de celui-ci mais aussi la dégradation des sols et déforestation liée à l'agriculture du soja.  
  
Il s'agit bien évidemment d'un sujet intéressant mais je dois avouer que j'étais un peu étonnée que la pollution soit presque le seul sujet qui ressortait de **tous** les objets des étudiants, pas uniquement de ceux de mon groupe.  
La pollution est un gros problème de notre société actuelle je ne le nie pas. Cependant, j'estime qu'il y a de nombreux autres sujets pour lesquels on devrait chercher des solutions et pour lesquels on aurai un **réel** impact.  
En effet, il a été prouvé que nous, communs des mortels qui ne possèdons pas de *jets privé*, ne pouvons pas changer grand chose si les millionnaires qui nous entourent refusent d'arrêter d'utiliser leurs jets pour un voyage de **20 kilomètres** à peine.  
Le changement que provoquerai **50% de la classe moyenne** faisant des efforts sur son taux personnel de pollution en réduisant, par exemple, sa consommation de viande ou en se mettant à utiliser un vélo plutôt qu'une voiture équivalerai à **1% des grands riches** faisant un effort à leur propre niveau, donc utiliser un peu moins leurs "*privilèges de riches*".  
  
Je m'égare un petit peu mais je tenais à partager mon avis car j'y ai beaucoup pensé tout au long de ce cours.
  
**2.** Quelles thématiques réunissent nos 5 objets ?  
  
Nous avons eu quelques minutes chronométrées pour noter, sur la grande page blanche autour de laquelle nous nous étions assis, toutes les idées qui nous passaient par la tête.  
*Pollution, argent, déchets, ...* Nous avons discuté tranquillement et avant même que les professeures ne nous annoncent la suite des activités, mon groupe et moi nous sommes déjà lancés dans l'exercice de lier nos objets à un sujet commun, à une thématique commune.  
Plus tard, Chloé qui animait le cours, a demandé que chaque membre du groupe note sur un post-it une thématique sur laquelle il aimerait travailler. Il fallait bien sûr un minimum de rapport avec nos objets.  
  
De là sont ressortis ces **_magnifiques_** post-it's !  
  
![](images/image-post-its1.jpg)

**3.** Choisir une thématique
  
Parmis ces différentes thématiques, il a fallut choisir une seule d'entre elle. Pour cela, nous avons suivis la technique proposée par Chloé. Il s'agissait d'une espèce de vote. Chaque membre devait noter chacunes des thématiques sur 4.  
  
* 1/4 : *je ne suis pas d'accord de travailler sur cette thématique*, 
* 2/4 : *je peux peux suivre le groupe si cette thématique remporte le plus de vote*,
* 3/4 : *je suis d'accord de travailler sur cette thématique*,
* 4/4 : *je suis très enthousiaste de travailler sur cette thématique*
  
| post-it |  4/4    |  3/4  |           2/4           | 1/4  |
|-----|-----------------|---------|--------------------------|--------|
| 1   | Mt Mr V    |  L A |    |       |
| 2   | Mt A V    |  L Mr |    |        |
| 3   | Mt Mr A V  |  L |    |        |
| 4   | L   |  Mr A | Mt V   |        |
| 5   | Mt L A  |  Mr V |    |        |
  
La thématique n°3, **"pollution liée au transport"**, fut l'heureuse gagnante !  
  
### 5.2.4 Quelles problématiques ressortent de cette thématique ?
  
Pour cette étape, il nous a été demandé de procéder d'une manière bien particulière. Pour prendre la parole et donner une idée de problématique concernant la **"pollution liée aux transports"**, nous devions commencer par dire *"Moi, à ta place, je..."*. C'était très perturbant de devoir commencer toutes nos phrases comme ça.  
J'ai souvent commencé à expliquer mon idée et puis en plein milieu de ma phrase je disais *"Ah mince, moi à ta place..."* et je continuais mon explication (Oupsyy).  
Mariam, lors de cette étape, était celle qui notait toutes les idées de problématiques et voilà ce qui en est ressorti :  
  
![](images/nos-probl%C3%A9matiques3.jpg)
  
### 5.2.5 Echanges avec d'autres groupes
  
Tout comme l'expression *"Au plus on est, au plus on rit"*, la dernière étape de ce cours s'est basée sur *au plus on est, au plus d'idées on trouve*.  
C'est pourquoi, nous avons dû choisir un représentant pour qu'il aille récolter des idées de problématiques auxquelles on aurai pas pensé. Virginie est celle qui a joué ce rôle dans notre groupe. Elle est allée voir deux autres groupes avec qui elle est restée 3 minutes à chaque fois et a noté tout ce à quoi nous n'avions pas pensé.  
  
![](images/autres-probl%C3%A9matiques2.jpg)
  
De notre côté, nous avons acceuilli durant 3 minutes, Chiara, qui avait comme thématique *"Vivre en harmonie avec les océans"*. Nous lui avons donné nos idées de problématiques en continuant d'utiliser la méthode *Moi, à ta place*.  
Montée des eaux, acidification de l'eau, baisse de la biodiversité, fonte des glaces, continents de déchets,... [Chiara](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/chiara.castrataro/) a noté nos nombreuses idées et est ensuite passée au groupe suivant, nous laissant avec [Karl](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/karl.preux/), représentant du second groupe auquel nous avons apporté nos idées concernant le gaspillage et le recyclage.  
  
### Mot de fin pour le cours 2
  
Ce qui fut compliqué pendant ce cours était le fait d'avancer *petit à petit*, de ne pas bruler les étapes et donc de ne pas rechercher tout de suite une **solution**. 

Nous sommes ainsi arrivés au bout de ce cours concernant le **module 5** et en sommes ressortis avec des *groupes pour notre projet final*, une *thématique* ainsi qu'une *liste de problématiques*.
   
## 5.3 Cours 3 : La dynamique de groupe
  
Lors de ce cours, Chloé Crokart nous a parlé de nombreuses techniques pour se lancer au mieux dans un travail de groupe. Dans ce point **5.3**, je vais décrire trois outils abordés lors de cette *mini formation*.  
  
### 5.3.1 La technique du doigt 
  
Comment faire pour qu'une personne n'accapare pas le temps de parole et que tout le monde donne son avis ?  
Plusieurs techniques nous ont été proposées comme, par exemple, le baton de parole, chronométrer le temps de parole ou encore faire un tour de tables. L'outil qui m'a le plus plu est : **la technique du doigt**.  
Le principe de cette technique est que lorsque que mon collègue (1) a la parole et que j'ai quelque chose à dire, je vais poser ma main sur la table en mettant **un doigt** en avant. Si quelqu'un d'autre (2) aimerai également parler, il placera sa main sur la table avec **deux doigts** en avant. Ainsi, la personne (1) ayant la parole saura que les autres aimerai réagir.  
Lorsque (1) aura terminé de parler, je prendrai la parole, enlèverai ma main de la table et la personne qui mettait **deux doigts** en avant ne montrera plus **qu'un seul doigt** pour indiquer qu'elle sera la prochaine à parler, un ordre de passage est donc établi en même temps.  

Cette technique évite de couper une personne qui parle et permet de montrer très clairement à celle-ci que les autres ont un avis, une réponse ou une idée à donner en plus d'établir un ordre de parole.  
   
### 5.3.2 Le vote pondéré
  
La prise de décision est un passage inévitable et compliqué lors d'un travail de groupe. Parfois, il vaut mieux *laisser décider une personne*, sur base de l'expérience de celle-ci, parfois il est plus intelligent de *ne pas prendre de décision* et parfois il est important de *prendre la décision ensemble*.  
  
![](images/art-de-d%C3%A9cider1.jpg)

Le vote à l'unanimité, le vote à la majorité, le vote classé,... sont différentes façon de s'y prendre. Moi, je vais parler du **vote pondéré**.  
  
Le vote pondéré consiste à donner plusieurs voix à chaque membre du groupe. Lorsqu'un choix entre différentes options doit être fait, chaque membre posera ses voix sur les options pour lesquelles il vote. La particularité est qu'il peut décider de mettre toutes ses voix sur la **même option** ! Ainsi, son vote aura un plus fort impact que s'il décidait de dispercer ses voix sur des options différentes.  
Cette outil permet aux membres de graduer leur choix et permet, au final, de choisir l'option remportant le plus de votes.
  
### 5.3.3 Feedbacks collectifs et individuels
  
Lorsqu'on travail en groupe, il n'est pas évident de dire certaines choses qui nous dérange dans le travail que l'on fait ni d'avoir confiance en ce qu'on fait. C'est pourquoi j'ai beaucoup aimé le principe du feedback collectif et individuel.  
Dans cet outil, le feedback est, en d'autres mots, une critique. Elle peut être positive, mettre en avant une force, ou négative, mettre en avant un risque ou un défi.  
Le principe est que chaque membre **doit** donner un feedback collectif et un feedback individuel. Cela force chacun des membres à donner un avis constructif sur le déroulement du projet.  
  
Le plus important, lors d'un feedback, est de toujours parler en "**Je**", de nommer des **faits**, de dire l'**émotion** que cela nous procure et de faire une **demande**.  
  
**_Exemple_** : J'ai remarqué qu'on commençait toujours nos réunions avec du retard (utilisation du pronom "**je**"). Aujourd'hui, vous êtes arrivés avec 15 minutes de retard (mise en avant des **faits**) et cela me dérange car je fais l'effort d'arriver à l'heure (je partage l'**émotion** que cela me procure). Je propose donc de changer l'heure de début de nos réunions à 15 minutes plus tard (Je fais une **demande**).  
  
Cette technique permet d'*améliorer* notre travail collectif et individuel mais permet également d'éviter la *création de tensions* entre les membres d'un groupe. Par conséquent, elle crée une atmosphère de **confiance** et d'**écoute** entre différentes personnes. 
  
### 5.3.4 Clotûre de la formation
  
En deux heures, cette formation nous a donné de nombreux outils qui nous permettront d'avancer au mieux avec des personnes nouvelles, venues d'études différentes, avec qui on pourrait ne pas s'entendre. Elle nous a donner les outils qui nous permettront de gérer des problèmes qui pourraient apparaitre tout au long de notre projet commun.  
  
Je pense qu'il est important d'essayer ces outils et de choisir ceux avec lesquelles on est le plus à l'aise. Il en existe tellement que l'on peut se perdre et abandonner leur utilisation avant même d'en voir les résultats sur la dynamique de groupe.  
