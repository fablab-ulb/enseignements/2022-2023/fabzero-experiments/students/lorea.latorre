# 2. Conception Assistée par Ordinateur (CAO)

Lors de ce module, j'ai appris à utiliser une application, appelée OpenSCAD. Il s'agit d'une application qui permet la création en trois dimmensions. L'objectif de ce module étant la création d'un code paramétrique [flexlinks](https://www.compliantmechanisms.byu.edu/flexlinks).


## 2.1 OpenSCAD

[OpenSCAD](https://openscad.org/) est un logiciel libre de modélisation paramétrique. Il permet la création en 3D d'objets du quotidien ou d'objets issus de notre imagination.  
  
Il est assez facile à utiliser grâce à l'aide de la [OpenSCAD CheatSheet](https://openscad.org/cheatsheet/) dont les informations les plus importantes selon moi sont repises ci-dessous :  
  
|  |   code   |  résultat/action  |
|-----|-----------------|---------|
| 2D   |   circle(radius)  | cercle dont le rayon vaut le chiffre mit entre parenthèse|
|  3D  |  sphere(radius)   |  sphère dont le rayon vaut le chiffre entre parenthèse|
|    | cube([x,y,z], center)  |  cube de côtés de longueurs x, y, z et dont le centre se trouve à un certain endroit du repère  |
|    | cylinder(h, r1,r2, center)   | cylindre de hauteur h et dont le cercle inférieur est de r1 et celui supérieur est de r2  |
| transformations   | translate([x,y,z])  |  la forme créée sera déplacée/translatée de, x sur l'axe x, y sur l'axe y et z sur l'axe z |
|    | hull () | les formes codées mises entre les parenthèses vont être rejointes (voir exemple plus loin) |
|     | difference()  | suppression de là où se croisent les deux formes |
  
### 2.1.1 A quoi cela ressemble?

Lorsque vous ouvrirez l'application, cette image apparaitra : 
   
![](images/openscad-acceuil.png)

Vous devrez simplement appuyer sur "*nouveau*" pour ouvrir une page dans laquelle, sur la *droite*, vous pourrez voir un graphique à 3 dimensions, en dessous de celui-ci, pleins de petits dessins qui désignent des actions que vous pourrez faire sur OpenSCAD, en dessous, deux cadres informatifs et sur la *gauche*, une page blanche avec un "1" désignant la première ligne de code, là où vous écrirez.  
  
### 2.1.2 Quelques exemples simples
  
* **Un cylindre :**  
  
      cylinder (10,5,3,true);  
  
  ![](images/openscad-cylindre.png)  
  
**True** est le moyen de dire que le centre de mon cylindre se trouve en (0,0,0).  
  
Pour que le cylindre soit **plus lisse** et **paramétrique**, le code ressemblera à ceci:  
  
  ![](images/openscad-cylindre-fn.png)  
  
* **Une toupie :**  
  
  ![](images/openscad-toupie.png) 
    
* **translate ([x,y,z]):**  

      translate([10,10,5]) sphere(r=10);  

  ![](images/translate-sphere.png) 
    
  Comme vous pouvez le voir, le centre de la sphère, initialement en (0,0,0), a été translaté de 10 sur l'axe des x, 10 sur l'axe des y et de 5 sur l'axe des z.
  
* **hull (){}:**  

  ![](images/hull-sphere.png)  

  Deux sphères qui initialement ne se touchent pas ont été rejointes par l'action *hull*
  
* **difference (){}:**  

  ![](images/difference-sphere.png)  

  Ici, la sphère croisait un cylindre et j'ai décidé de faire la différence de l'un de l'autre, c'est à dire de supprimer/retirer le cylindre de la sphère.

    
## 2.2 Flexlinks

### 2.2.1 Les mécanismes flexibles  
  
  Appelés *compliant mecanism* en anglais, il s'agit de mécanismes qui tirent au moins une partie de leur mobilité de la déviation d'éléments flexibles à la place de joints mobiles.  

  Principaux avantages des mécanismes flexibles :  
    
  * objet constitué d'une seule pièce  
  * processus de production simple  
  * fabrication peu couteuse  
  
  Pour plus d'informations à ce sujet, je vous invite à regarder la vidéo "[Pourquoi les machines qui courbent sont meilleures](https://youtu.be/97t7Xj_iBv0)" publiée sur Youtube par la chaine *Veritasium*.

### 2.2.2 Mon kit Flexlinks  
  
Voici comment est ce que j'ai créé le kit Flexlinks suivant :  
  
  ![](images/openscad-r%C3%A9sultat-code-lego.png)
  
**1.** J'ai commencé par créer deux cylindres :  
  
  ![](images/openscad-code1.png)  

**2.** Ensuite, je les ai *combinés* :  
  
  ![](images/openscad-code2.png)  

**3.** J'ai refait les deux mêmes étapes mais translaté en *y* :  
  
  ![](images/openscad-code3.png)  

**4.** J'ai rejoint mes deux formes grace à une tige cubique :  
  
  ![](images/openscad-code4.png)  
  
**5.** J'ai choisi mes paramètres :  
  
  ![](images/openscad-code5.png)  
  
**6.** J'ai fait l'union de tout cela avec quatres nouveux cylindres :  
  
  ![](images/openscad-code6.png)   

Dans mon [module03](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/fabzero-modules/module03/), vous pourrez voir comment j'ai modifié mon code afin de pouvoir le combiner avec la pièce codée par [Matthew Doyle](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/).  
  
  ![](images/photo-flexlinks3.jpg)
  
  
## 2.3 Autres outils pour la conception sur odinateur  
  
Il existe de nombreux autres logiciels permettant de s'amuser avec le 3D ou le 2D sur son ordinateur. Les logiciels qui m'ont été proposés, autres que OpenSCAD sont **Inkscape**, logiciel de dessin vectoriel (2D), ainsi que **FreeCAD**.  
  
Si cela t'intéresse, voici une [vidéo tuto](https://youtu.be/GSGaY0-4iik) qui t'apprendra les bases d'Inkscape pour pouvoir te lancer dans la création 2D ! 
  
J'ai personnellement travaillé avec OpenSCAD car j'ai facilement compris comment l'utiliser. De plus, je n'ai jamais réussi à ouvrir le programme FreeCAD. Je l'ai installé mais lorsque je voulais l'ouvir, on me demandait de le réinstaller... Très bizarre mais je n'ai pas cherché plus loin car OpenSCAD m'apporte déjà tout ce dont j'ai besoin.
  
  
## 2.4 Les licences    
  
### 2.4.1 Licences CC  

*"Les licences Creative Commons donnent à tous, des créateurs individuels aux grandes institutions, un moyen standardisé d’accorder au public la permission d’utiliser leur travail créatif en vertu de la loi sur le droit d’auteur."*  
Plus simplement, cela répond à la question : *Qu'est ce que je peux faire avec cette oeuvre?*  
  
**CC PAR**, **CC BY-SA**, **CC BY-NC**, **CC BY-NC-SA**, **CC BY-ND** et **CC BY-NC-ND** sont les 6 types de licences *Creative Commons*. Elles diffèrent parfois de peu mais il est important de bien choisir la licence que vous déciderez d'attribuer à votre travail.  
Il s'agit tout de même de choisir comment les autres pourront **utiliser**, **modifier** ou même **commercialiser** votre travail.  
  
Vous vous demandez quelle licence choisir ? Ce [site](https://creativecommons.org/about/cclicenses/) explique clairement les différents types de licences et vous propose un outil d'aide pour choisir celle qui vous convient.
  
### 2.4.2 Autres licences  

Il existe énormément d'autres licences mais celles qui nous intéressent dans ce cours sont celles qui remplissent les critères du *libre*.  
Vous trouverez [ici](https://www.gnu.org/licenses/license-list#GNUGPL) une liste des licences de logiciels libres compatibles avec la GPL et sur [ce site](https://opensource.org/licenses/), les licences approuvées par OSI, l'*Open Source Initiative*.  
  
### 2.4.3 Comment licencier mon code OpenSCAD ?  
  
Pour cela, il faut clairement écrire dans votre code la licence que vous avez choisie avec un lien menant à celle-ci.  
    
Simplement comme ceci :  
  
  ![](images/openscad-licence.png)  
    
  