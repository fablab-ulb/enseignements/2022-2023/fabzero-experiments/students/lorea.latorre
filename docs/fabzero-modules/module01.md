# 1. Gestion de projet et documentation  
  
Lors de cette première semaine, j'ai appris les bases utiles pour ce cours. Pour cela, j'ai dû commencer par installer de nombreux programmes et j'ai dû apprendre à les utiliser à l'aide de tutos et d'entrainements.

## 1.1 Installation de différents programmes sur mon Windows 11

* Linux : Ubuntu
* VS Code
* Gimp
* Git

### 1.1.1 Linux
  
Pour **installer** Linux j'ai suivis [ce tuto](https://youtu.be/sUsTQTJFmjs).

Etapes importantes par lesquelles je suis passée :

* Activer le sous-système Windows pour linux :  

![](images/sous-système-windows-pour-linux.png)  
  
* Installer Ubuntu :  
  
![](images/logo-ubuntu.png)

**_Conseil_** : Faire attention lorsqu'il faut créer un mot de passe pour Ubuntu ! En effet, rien ne s'affiche lorsqu'on tappe notre code.  
  
**Apprentissage des lignes de commandes Linux**  
  
J'ai appris les bases de l'utilisation de Linux grâce à [ce site](https://ubuntu.com/tutorials/command-line-for-beginners#1-overview).  
En une heure, il nous permet de connaitre les fondamentaux pour naviguer dans notre explorateur de fichiers.  
  
**_Principales commandes :_**  
  
| Commande | Action |
| ------ | ------ |
| pwd       |    affiche le chemin pour parvenir où je suis    |
| cd       |  change de dossier      |
| cd .. | revient au dossier parent|
|ls |montre les fichiers et dossiers |
| whoami | rappelle notre username |
| cat | montre l'intérieur du fichier |
| echo | écrit dans le fichier |
| less | entre dans le fichier |
|mv | renomme un fichier soit le déplace |
| touch | crée un fichier |
|rm | supprime un fichier |
| mkdir| crée un dossier |
| rmdir | supprime un dossier vide |
rm -r | supprime un dossier même s'il contient des fichiers | 
      
### 1.1.2 VS Code  
  
*Visual Studio Code* est un éditeur de texte qui permet l'utilisation de l'écriture en markdown, écriture plus rapide et plus pratique que l'HTML.   
J'ai directement **installé** VS Code depuis Microsoft Edge.
   
### 1.1.3 Gimp  
  
Gimp est un outil pour éditer et retoucher des images. Il est utile lorsqu'on a besoin de réduire la qualité d'une image afin qu'elle prenne moins de place dans notre stockage ou encore réduire la taille de l'image.  
Pour **installer** Gimp, je me suis rendue dans *Microsoft Store* et j'ai recherché l'application *Gimp*. J'ai ensuite cliqué sur *installer*.  
  
![photo](images/logo-gimp.png)

### 1.1.4 Git
  
Git est un logiciel de gestion de versions décentralisé.  
Il va nous permettre de modifier nos documents Gitlab à travers lui. C'est à dire qu'au lieu de faire nos modifications sur gitlab, nous les feront sur notre éditeur de texte, par exemple VS Code, et grâce aux commandes git, on va push nos modifications sur gitlab.  
Ce n'est pas très clair mais vous comprendrez mieux une fois que vous aurez lu la totalité de ce premier module.
  
**1.** Installer Git  
  
Pour **installer** Git, j'ai simplement suivi [ce tuto](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#install-git).
  
Pour vérifier que git est bien installé, faites ceci :  
  
![](images/Confirmation-git-install%C3%A9.png)  
  
**2.** Configurer Git  
  
Pour **configurer et apprendre** à utiliser Git, j'ai utilisé [ce tuto](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#configure-git). 

## 1.2 Créer notre paire de clés SSH  
  
En quelques mots, les clés SSH permettent de **lier** notre ordinateur à la *base* Gitlab. En créant les clés gitlab, on crée une clé privée ainsi qu'une clé publique. On va copier la clé **publique** dans notre compte Gitlab pour créer le lien.  
Pour **créer** mes clés, j'ai suivis les étapes expliquées sur [cette page](https://docs.gitlab.com/ee/user/ssh.html). Elle nous explique également comment entrer notre clé publique dans Gitlab.  
  
J'ai personnellement eu un problème lors de cette étape car je n'ai pas utilisé le tuto ci-dessus directement. J'ai d'abord suivi un tuto youtube qui a fait que j'ai créé une clé SSH de nom différent à celui attendu pour ce cours. Par conséquent, mon terminal ne *trouvait* pas mes clés SSH et j'ai eu besoin de l'aide des professeurs pour, premièrement, comprendre ce qui n'allait pas et ensuite comprendre comment régler mon problème, c'est à dire, refaire les étapes, en suivant le bon tuto et remplacer ma mauvaise clé par la bonne.  
  
Ces clés nous amènent à la prochaine et dernière étape : Cloner notre projet !

## 1.3 Cloner mon projet  
  
Le système de clonage nous permet d'avoir une copie de notre projet sur notre ordinateur et de pouvoir travailler dessus depuis celui-ci.  
Graçe aux commandes git, il nous sera possible de _push_ notre avancement sur gitlab sans même devoir ouvrir la page.
Pour cloner mon projet, j'ai eu besoin de l'aide des professeurs mais j'ai principalement suivi le processus donné sur [cette page](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html#clone-via-ssh). 
  
**_Commandes git pour push notre projet sur gitlab :_**
  
| Commande | Action |
| ------ | ------ |
| git pull | Télécharge les dernières modifications ayant pu être exercées sans qu'on le sache |
| git status | Indique le statut de notre projet |
| git add <nom du fichier modifié> | Ajoute les modifications exercées sur le fichier nommé |
| git add -A | Ajoute toutes les modifications |
| git commit -m "commentaire" | Permet de commenter notre modification |
| git push | Envoie les modifications du projet sur gitlab |  
  
Sur l'image ci-dessous, vous pourrez voir l'ordre dans lequel les commandes sont utilisées afin de faire passer les modifications effectuées sur notre ordinateur sur notre blog (sur gitlab).  
  
**_ATTENTION_** : Avant de faire n'importe quelle sauvegarde, TOUJOURS tapper **_git pull_** pour être sûr que nos documents soient les mêmes sur gitlab et sur notre ordinateur. Plus simplement, cette commande transmet toutes les potentielles modifications faites sur gitlab au clone sur votre ordinateur.  
  
![](images/commandes-git-mod1.png)
  
## 1.4 Problème possible
  
Il est possible d'oublier de *pull* des modifications de gitlab avant de *push* des modifications faites sur notre clone. Ceci est un problème par lequel je suis passée. Sur votre terminal, si cela vous arrive, vous verrez ceci :  
  
![](images/probl%C3%A8me-git-push.png)  
  
Pour régler ce problème, je me rappelle avoir du utiliser différentes commandes :  
  
    git remote -v 
  
    git branch
  
    git merge  
  
Je n'ai malheureusement pas pris de screens shots et ne me rappelle plus exactement de comment j'ai réglé ce problème... Je préfère ne pas expliquer comment faire sur bases de souvenirs peu certains. Je vous renvoie vers vos professeurs qui sont tout de même là pour vous aider mais aussi vers les autres étudiants qui suivent ce cours avec vous. Dans mon cas, c'est un étudiant qui m'est venu en aide lorsque je suis restée coincée à cause de ce problème. N'ayez pas peur, je suis sûre qu'ils seront ouverts à vous aider !  
  
**_RAPPEL_**, pour éviter ce problème, toujours utiliser la commande **_git pull_** avant de se lancer dans la sauvegarde de nouvelles modifiactions de votre clone !
  
## 1.5 Mettre à jour mon blog
  
Pour mettre à jour mon blog autrement qu'en attendant les nuits du lundi au mardi et du mercredi au jeudi, je peux le faire manuellement, je peux **forcer** la MAJ.  
  
**Comment faire ?**  
  
* Pour cela, je me rend sur ma page [gitlab](https://gitlab.com/). 
  
* Je clique sur **CI/CD**
  
![](images/maj-ci-cd1.png)
  
* Ensuite je clique sur **Schedule**, là je peux voir que des MAJ automatiques ont effectivement lieu durant les nuits de lundi à mardi et de mercredi à jeudi.
  
![](images/maj-schedule1.png)
  
* J'appuie sur un des boutons *play*
  
![](images/maj-play.png)
  
* Ensuite je vais dans **Pipelines**
  
![](images/maj-pipelines.png)
  
* Dans **Pipelines**, je peux voir l'historique des MAJ et donc vérifier que j'ai bien forcé une nouvelle MAJ.
  
![](images/maj-v%C3%A9rification.png)
  
Voilà, c'est la fin de ce premier module ! N'hésite pas à aller voir mon [module02](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/fabzero-modules/module02/) concernant la conception assistée par ordinateur !  