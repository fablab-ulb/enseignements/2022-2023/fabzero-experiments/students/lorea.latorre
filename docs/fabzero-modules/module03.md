# 3. Impression 3D

Cette semaine, j'ai appris à convertir mon fichier OpenSCAD, contenant mon code, afin de pouvoir l'imprimer à l'aide d'une imprimante *3D Prusa* présente dans le Fablab de l'université.  
L'objectif de cette semaine était donc de nous former à l'utilisation de l'imprimante 3D mais aussi d'identifier les avantages et les limites de ce type d'impression.

## 3.1 PrusaSlicer
  
*Prusa* est la marque de l'imprimante que l'on a appris à utiliser et un *Slicer* est un logiciel de génération de trajectoire d’outil utilisé dans la majorité des processus d’impression 3D pour la conversion d’un modèle objet 3D en instructions spécifiques pour l’imprimante.
  
### 3.1.1 Téléchargement
  
**PrusaSlicer** est donc l'application de la marque Prusa qui permet de convertir notre code en instructions concrètes pour l'imprimante que l'on va utiliser. Il est essentiel de télécharger [cette application](https://help.prusa3d.com/article/install-prusaslicer_1903) pour passer à la suite de ce module.
  
### 3.1.2 Ouvrir fichier dans PrusaSlicer

**Attention**, pour faire cette action, vous devez avoir enregistré votre code *OpenSCAD* en tant que fichier **.stl**. Ce n'est pas compliqué, voici comment faire :
  
  * Ouvrez votre fichier dans OpenSCAD et cliquez sur le bouton :  
    
    ![](images/transf-scad-en-stl-im1.png)
    
  * Cliquez ensuite sur ceci :  
    
    ![](images/transf-scad-en-stl-im2.png)

  * Enregistrez le document là où vous pourrez facilement le retrouver :
    
    ![](images/transf-scad-en-stl-im31.png)

Une fois ceci fait, vous avez converti votre code en un fichier **.stl** que vous pourrez ouvrir dans l'application **PrusaSlicer**. Comment faire?  
   
A nouveau, ce n'est pas difficile, voici les étapes à suivre:
  
**1.** Ouvrez l'application PrusaSlicer sur votre ordinateur  
**2.** Cliquez sur :
    
  ![](images/prusa-page-vierge1.png)

**3.** Choisissez le fichier **.stl** correspondant à votre code :
    
  ![](images/prusa-ouvrir-fichier1.png)
  
Une fois ouvert, vous pourrez voir votre objet codé sur le programme **Prusa**.  
  
  ![](images/prusa-fichier-ouvert1.png)  
  
### 3.1.3 Préparer à l'impression  
  
Maintenant que votre fichier est ouvert dans **PrusaSlicer**, il y a quelques actions à effectuer avant de pouvoir passer à _L'étape_ d'impression.  
Pour ce faire, j'ai suivis [ce tuto](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) qui explique comment faire cela en plus de donner quelques bases pour intégrer au mieux les différents outils qu'offre cette application.  
  
**1.** La première chose à faire une fois votre fichier ouvert, c'est d'**orienter** le modèle de façon à ce que la surface la plus grande soit à plat, c'est à dire en contact avec le plateau. Pas d'inquiétudes à avoir, **PrusaSlicer** vous propose deux boutons qui le feront pour vous !  
  
  ![](images/prusaslicer1.png)  

ensuite 

  ![](images/prusaslicer2.png)

**2.** Il faut spécifier **quelle imprimante** vous allez utiliser.  

  ![](images/prusaslicer3.png)

**Attention**  
  
  ![](images/prusaslicer4.png)
  
**3.** Il faut ensuite choisir le **filament** avec lequel notre imprimante fonctionne, moi j'ai choisi *"Generic PLA"*  
  
**4.** La prochaine étape est le choix de la **hauteur des couches**  
  
  ![](images/prusaslicer5.png)  

**5.** Choix des **supports** et de la densité de **remplissage**. Attention, les supports ne sont pas obligatoires et le remplissage est par défaut d'une densité de 15%. De plus, ces options prolongent le temps d'impression. 
   
**6.** Choix de jupe et bordure. La jupe sert de test avant impression pour vérifier la bonne adhérence au plateau et la bordure permet d'augmenter cette adhérence.  
    
**7.** Modifier l'épaisseur des parois est une option qui permet de les rendre plus solide si on change le réglage initial "2" par "3".  
  
**8.** Cliquer sur le bouton ***"Découper maintenant"*** pour voir une estimation du temps d'impression. S'il est trop long, vous pouvez toujours revenir en arrière et modifier des paramètres (ex : baisser densité de remplissage).  
  
**9.** Une fois que l'estimation de temps vous convient, vous pouvez **Exporter le G-code** et l'enregistrer sur une carte SD.  
  
  ![](images/prusaslicer6.png)  
  
**Conseil**, créez un fichier à votre nom dans cette carte SD pour retrouver plus facilement votre fichier une fois la carte insérée dans l'imprimante.  
  
### 3.1.4 Lancer l'impression  
  
Comment enfin lancer l'impression de votre code? 
  
  * Il faut avant tout **vérifier** que le filament installé correspond à celui dans le slicer et **nettoyer** délicatement le plateau à l'aide de désinfectant et d'un mouchoir.
  * **Insérer** la carte SD dans le panneau de commandes.
  * **Sélectionner** le fichier G-code à l'aide du bouton rond à droite de l'écran.
  * **Attendre** la fin du pré-chauffement et de la calibration pour voir si la première couche adhère bien au plateau.

**Attention**, pour éviter les quelques problèmes courants que l'on peut avoir lors de l'impression 3D, je vous invite à aller consulter le tuto mentionné plus haut.  
  
  
## 3.2 L'impression 3D

Les **avantages** :
  
  * accessible à tous
  * impression rapide
  * peu coûteuse
  * offre grande liberté dans nos créations
  * peut réparer des objets cassés
  
Les principales **limites** de l'impression 3D sont la *taille* et la *matière* utilisée. Le problème de la taille peut tout de même être solutionné par une impression en plusieurs fois. Au niveau de la matière, les imprimantes utilisent généralement le **plastique** car il fond à une température beaucoup plus basse que le métal. Une autre limite de l'imprimante 3D est le fait qu'elle est capable d'imprimer correctement dans le *"vide"* jusqu'à environ 75°C.  
  
L'impression de l'objet ci-dessous sert à tester les capacités et limites d'impression des imprimantes 3D.
  
  ![objet](https://cdn.thingiverse.com/renders/5c/56/f5/9c/33/567bc11d7aec4426905f458449cc690e_preview_featured.jpg)

## 3.3 Mon expérience

Dans le [module 2](https://gitlab.com/fablab-ulb/enseignements/2022-2023/fabzero-experiments/students/lorea.latorre/-/blob/main/docs/fabzero-modules/module02.md), j'ai dû créer un kit Flexlinks que je devais imprimer cette semaine.  
  
Après avoir discuté avec mes camarades de classe, j'ai remarqué que la grande majorité d'entre eux avait codé des objets avec uniquement des trous, donc des objets dans lesquelles peuvent s'emboiter uniquement des embouts qui entrent dans ces trous. Moi qui, au contraire, n'avait fait que des embouts, j'ai eu envie de donner à mon objet une possibilité supplémentaire, c'est à dire que j'ai décidé de modifier mon code pour qu'il ai un trou et 3 embouts.  

De plus, j'ai dû effectuer des modifications sur mon code pour pouvoir le faire concorder avec celui de quelqu'un d'autre puisqu'il s'agissait d'un des objectifs de ce module.  
  
### 3.3.1 Modifications de mon code
  
Mon code _initial_ était :
  
    // File : openscad-code-simple-lego-.scad
    // Author : Lorea Latorre
    // Date 01-03-2023
    // Licence : Creative Common Attribution-Non Commercial 4.0 International [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

    $fn=100;
    //cylindres principaux 
    e=2;
    r1=10;
    r2=10;
    //cylindres union
    c1=5.45;
    c2=5.45;
    //cylindre difference
    d1=5.55;
    d2=5.55;
    //translation
    tx=20;
    ty=50;
    tz1=-1;
    tz2=3;
    L=50;
    l=3;
     union (){
      hull (){
      cylinder (e,r1,r2,true);
      translate ([tx,0,0]) cylinder (e,r1,r2,true);   
    }
    translate ([(tx/2)-1,0,tz1]) cube ([l,L,e]);
      hull () {
          translate ([0,ty,0]) cylinder (e,r1,r2,true);
          translate ([tx,ty,0]) cylinder (e,r1,r2,true);
    }
    cylinder (e*3,d1,d2,true);
    translate ([tx,0,tz2]) cylinder (e*3,c1,c2,true);
    translate ([0,ty,tz2]) cylinder (e*3,c1,c2,true);
    translate ([tx,ty,tz2]) cylinder (e*3,c1,c2,true);
    }
  
Je l'ai _transformé_ en ceci :  
  
    // File : openscad-code-simple-lego-.scad
    // Author : Lorea Latorre
    // Date 08-03-2023
    // Licence : Creative Common Attribution-Non Commercial 4.0 International [CC BY-NC 4.0](https://creativecommons.org/licenses/by-nc/4.0/)

    $fn=100;
    //cylindres principaux 
    e=2;
    r1=5;
    r2=5;
    //cylindres union
    c1=2.45;
    c2=2.45;
    //cylindre difference
    d1=2.55;
    d2=2.55;
    //cube
    L=50;
    l=3;
    difference (){
      union (){
      hull (){
      cylinder (e,r1,r2,true);
      translate ([r1*2,0,0]) cylinder (e,r1,r2,true);   
    }
    translate ([(r1)-(l/2),0,-e/2]) cube ([l,L,e]);
      hull () {
          translate ([0,L,0]) cylinder (e,r1,r2,true);
          translate ([r1*2,L,0]) cylinder (e,r1,r2,true);
    }
    translate ([r1*2,0,(e*2)/2]) cylinder (e*2,c1,c2,true);
    translate ([0,L,(e*2)/2]) cylinder (e*2,c1,c2,true);
    translate ([r1*2,L,(e*2)/2]) cylinder (e*2,c1,c2,true);
    }
    cylinder (e*3,d1,d2,true);
    }
  
Voici l'objet résultant de ce code :  
  
  ![](images/openscad-r%C3%A9sultat-code-avec-matthew.png)

Vous l'avez peut-être constaté, ces deux codes ne possèdent pas les mêmes paramètres. En effet, en faisant ces modifications, je me suis remises en question, j'ai appris à mieux maîtriser le logiciel et ai donc appris à mieux *paramétrer* mon code ce qui fait que le code final est bien plus simple que l'initial. Hors, dans les **faits** mon objet final a acquis une particularité supplémentaire et n'est pas plus simple que mon objet initial.  
  
### 3.3.2 Le code de Matthew Doyle
  
[Matthew](https://fablab-ulb.gitlab.io/enseignements/2022-2023/fabzero-experiments/students/matthew.doyle/) est l'étudiant avec qui j'ai fait concorder mes paramètres pour que nos objets respectifs puissent s'emboiter.  
  
Voici à quoi ressemble **son** code :  
  
  ![](images/code-matt.png)

Cela donne un objet très ressemblant du mien mais qui ne possède que des trous.  
  
  ![](images/objet-matt.png)

Comment avons nous su quels paramètres donner pour que nos objets respectifs s'emboitent?  
  
Il a fallut faire des tests !  
  
Des élèves de la classes ont codé des objets qui servent justement à cela.  

  ![](images/test-imprimante3d.jpg)  

Grâce à un objet possédant des trous de différents rayons et d'autres possédants des embouts de rayons différents, des élèves ont pu tester quels mesures faisaient *combos*.  
C'est ainsi que l'on a su combien devaient mesurer les rayons des embouts et des trous que l'on voulait emboiter.  
  
### 3.3.3 Résulat
  
  ![](images/photo-flexlinks3.jpg)
  
Notre objet final est donc celui-ci. Il est capable de se courber et de tourner au niveau du lien embout/trou. Grâce au nombreux autres trous et embouts, d'autres pièces peuvent également venir s'y fixer pour ajouter des fonctions à notre objet.  
Personnellement, je pense que la meilleure fonction que puisse avoir notre objet est de créer le lien entre deux objets plus imposants qui, grâce à la possibilité de **tourner** et de **se courber** de notre objet, pourront effectuer des mouvements qui augmenteront l'efficacité de leurs propres fonctions. 
