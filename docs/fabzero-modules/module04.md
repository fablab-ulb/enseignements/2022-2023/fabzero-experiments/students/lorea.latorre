# 4. Outil Fab Lab sélectionné

Lors des deux semaines dédiées au module04, j'ai été formée à l'utilisation d'une fraiseuse CNC.

## 4.1 Avant le cours : Installation de Fusion 360

Avant de se rendre à la formation, il nous a été demandé d'installer **Fusion 360** sur notre ordinateur, *une plate-forme logicielle 3D cloud de modélisation, de CAO, de FAO, d’IAO et de conception de circuits imprimés destinée au design et à la fabrication de produits*. *"Cloud"* signifiant que votre projet peut être partagé en toute sécurité avec toute personne interne ou externe à votre entreprise.

### 4.1.1 Créer un compte Autodesk

[Autodesk](https://www.autodesk.com/) est une société d'édition de logiciels de création et de contenu numérique dont, entre autres, **Fusion 360**.  
Pour pouvoir installer la plate-forme fusion 360, il est donc obligatoire de *créer un compte gratuitement sur Autodesk*. 
Pour cela, j'ai effectué les étapes ci-dessous:
  
1. j'ai **cliqué** sur [ce lien](https://www.autodesk.com/education/edu-software/overview?sorting=featured&filters=individual) 
2. j'ai été dirigée vers une page dans laquelle **Fusion 360** était indiqué, j'ai **cliqué** sur *get product* desous.  
  
![](images/mod4-fusion360-get-product.png)
  
3. On m'a alors demandé de créer un compte Autodesk pour lequel j'ai dû **entrer** les informations suivantes : *mon nom, prénom, adresse mail et date de naissance*.  
  
4. On m'a également demandé de **créer** un *mot de passe*  
  
5. Pour **finaliser** la création du compte, j'ai dû me rendre sur ma boîte mail et ouvrir le courriel envoyé par Autodesk afin de **cliquer** sur *Verify email*.  
  
Ainsi, j'ai terminé de créer mon compte Autodesk et j'ai pu accéder au téléchargement de **Fusion 360**.  
  
### 4.1.2 Télécharger Fusion 360
  
Pour télécharger la plate-forme, je suis retournée sur [ce lien](https://www.autodesk.com/education/edu-software/overview?sorting=featured&filters=individual) et me suis **connectée** à mon compte Autodesk.  
  
* J'ai à nouveau **cliqué** sur *get product* puis sur *Accès*.
  
![](images/mod4-autodesk-acc%C3%A8s-fusion360.png)
  
* J'ai été redirigée vers une nouvelle page dans laquelle *mon nom, prénom et date de naissance* m'**ont été redemandé**.  
* Une fois ces informations remplies, on **m'a demandé** mon *statut* et j'ai dû choisir *"étudiant"* j'ai dû spécifier que j'étais *étudiante à l'université*, que j'étais à l'*Université Libre de Bruxelles* et ai même dû noter ma *date d'inscription* et la *date de graduation* de mes études. 
  
* Une fois tout cela validé, j'**ai reçu** le message ci-dessous et le téléchargement de Fusion 360 a débuté.  
  
![](images/mod4-autodesk-confirmation1.png)

### 4.1.3 Ouverture de l'application 
  
* Une fois téléchargé, j'ai ouvert **Fusion 360** et sa configuration a commencé.
  
![](images/mod4-fusion360-configuration1.png)
  
* J'ai alors dû me **connecter** à mon compte avec mon *adresse mail* ainsi que mon *mot de passe*.  
  
* Une fois connectée, j'ai reçu le message ci-dessous et ai **cliqué** sur "*Suivant*".
  
![](images/mod4-autodesk-msg-sur-fusion3601.png)
  
* Le message suivant est alors apparut et ne sachant pas ce que je devais faire, j'ai préféré attendre le cours pour demander quoi faire au professeur. Je voulais éviter toute erreur et complication.  
  
![](images/mod4-autodesk-cr%C3%A9er-%C3%A9quipe1.png)
   
## 4.2 Cours 1 : Première partie de la formation
  
La toute première chose que j'ai faite a été de demander ce que je devais faire suite au message reçu ci-dessus. Le formateur était assez étonné que je reçoive ce message car il ne l'avait jamais vu avant mais comme tous les élèves de la formation l'avaient reçu, il nous a simplement dit de créer une équipe appelée fablab. Une fois fait, nous avions enfin accès au programme **Fusion360** !  
  
Une fois que je suis entrée dans le programme, un des formateurs m'a donné une clé USB qui contenait l'objet sur lequel on allait apprendre à utiliser le programme. J'ai dû copier le document concerné sur mon ordinateur et l'ouvrir depuis Fusion360.  
  
**_Conseil_** : travailler avec une souris sur Fusion360 est bien plus simple que de travailler avec, uniquement, l'ordinateur. 
  
### 4.2.1 Apprendre comment utiliser Fusion360 :
  
#### 4.2.1.1 **Ouvrir** le programme et **ouvir** un document :  

  Avant toute chose, j'ai dû ouvrir le programme Fusion360 et ensuite ouvrir le document donné par les formateurs. Pour ouvrir ce document, j'ai cliqué sur le bouton tout en haut à gauche appelé *fichier* et ensuite sur *ouvrir*.  
      
  Une nouvelle page est apparue et j'ai cliqué sur *"ouvrir à partir de mon ordinateur"*, bouton en bas à gauche de la nouvelle page. Mon bureau s'est ouvert et j'ai pu choisir le document que je désirais ouvrir.   
    
    
#### 4.2.1.2 **Mettre** en mode "Fabriquer" :  
  
  Une fois mon document ouvert dans Fusion360, j'ai commencé à apprendre comment *créer* les instructions que la fraiseuse lira pour fraiser dans le bois. Pour cela, il faut se rendre dans le mode **Fabriquer**. Quand on ouvre Fusion, le programme est automatiquement sur le mode **Conception**, mode sur lequel on crée l'objet qu'on désire fraiser. Pour ce premier cours, les formateurs avaient déjà conçu une pièce sur laquelle on allait apprendre différents techniques, différentes façons de créer les instructions pour la machine CNC.
  Pour passer au mode **Fabriquer**, j'ai dû cliquer sur le bouton *Conception* et ensuite sur *Fabriquer*.

#### 4.2.1.3 **Configurer** les axes :  
  
  Ceci est une étape très importante, j'ai dû configurer que l'axe x était la longueur de mon objet, l'axe y était la largeur et l'axe z la hauteur. Pour cela, j'ai d'abord dû cliquer sur *configuration* et ensuite sur *Nouvelle configuration*.  
      
  ![](images/fusion360-configuration1.png)  
    
   Cela a ouvert un nouvel onglet : 

  ![](images/fusion360-configuration2.png)  
     
   Dans cet onglet, il y a plusieurs choses à faire :  
     
  * changer l'orientation et choisir *Sélectionner l'axe/le plan Z et l'axe X*  
    
  ![](images/fusion360-configuration3.png)  
    
  * sélectionner l'axe Z en cliquant sur une droite de notre objet désignant l'épaisseur de celui-ci.  
    
  ![](images/fusion360-configuration4.png)   

  * sélectionner l'axe x en cliquant sur une droite correspondant à la longueur de l'objet.  

  ![](images/fusion360-configuration5.png)  

  Quand l'axe est sélectionné, il devient bleu, cela me permet de vérifier que j'ai choisi le bon axe.  

  **_Attention_** : lors du choix de nos axes, il faut vérifier que Z aille bien vers le haut et X vers la droite (il est important de bien voir le sens dans lequel est notre objet pour ne pas se tromper entre la gauche et la droite). Si jamais les vecteurs ne sont pas dans le bon sens, il suffit de cliquer sur le bouton *Inverser l'axe Z* ou bien *Inverser l'axe X*, selon l'axe qui n'est pas dans le bon sens, pour que ceux-ci changent de sens.  
    
  ![](images/fusion360-configuration6.png)  
    
   * sélectionner le bon point d'origine. Il faut vérifier que l'option *Origine* soit bien sur *Point sur le brut*. Une fois ceci fait, j'ai cliqué sur *point de bloc* et des points sont apparus sur mon objet.  
   
  ![](images/fusion360-configuration7.png)
    
   J'ai dû choisir le point le plus haut du centre de mon objet.  
       
  ![](images/fusion360-configuration8.png)  

  **_Attention_** : pour pouvoir bouger le point de vue sur l'objet sans souris, il faut cliquer sur le carré en haut à droite. Ce n'est vraiment pas pratique mais il s'agit du seul moyen.  

#### 4.2.1.4 **Configurer** le brut :

   Une fois les axes définis, il est important de dire à la machine sur quoi elle va travailler, dans mon cas, sur quelle planche de bois elle va fraiser. Cette planche de bois est appelée le **brut**. Toujours dans l'onglet de configuration, en cliquant sur le petit cube jaune en haut à gauche et si je choisis ensuite le mode *boite à taille fixe*, je peux configurer les mesures du brut que j'ai choisi.  
  
  ![](images/fusion360-configuration9.png)  
    
   Une fois mes mesures définies, je peux cliquer sur **OK** en bas à droite de l'onglet car j'ai terminé de tous configurer !
    
#### 4.2.1.5 **Définir** les fabrications :  
  
  Par fabrications, je parle des instructions pour que la machine comprenne qu'elle doit fraiser à tel ou tel endroit. Elles sont nombreuses, en 2D ou en 3D, mais pendant la formation nous nous sommes focalisé sur celle en 2D. 

![](images/fusion360-actions1.png)  

  Ce qui m'a plu sur le programme est que si je pose ma souris sur une option, par exemple **Usinage adaptatif 2D**, une page s'ouvre et explique de quoi il s'agit.  

![](images/fusion360-actions2.png)  

**Exemples d'actions :**

* **Usinage adaptatif 2D** et **Poches 2D** : ces deux options sont très ressemblantes car il s'agit des instructions pour que la fraiseuse *"creuse"* dans le brut.  
Ce qui les différencie est que l'option **poches 2D** fait que la fraise descende petit à petit en Z alors que **l'usinage adaptatif** lui peut descendre, petit à petit, en Z mais également venir fraiser tout de suite depuis l'extérieur du brut. C'est un peu compliqué mais la chose à retenir est que l'usinage adaptatif peut faire tout ce que font les poches 2D et est plus rapide !  
  
Pour configurer ces options, il faut exactement faire la même chose, que ce soit pour usinage adaptatif ou pour poches 2D, voici les étapes que j'ai suivies :  
  1) J'ai d'abord dû cliquer sur le bouton **2D**, puis choisir *usinage adaptatif 2D* ou *Poches 2D*.  
  2) Un nouvel onglet s'est ouvert et la première chose que j'ai dû faire a été de choisir l'outil avec lequel je voulais travailler.  
    
  ![](images/fusion360-actions3.png) 
    
  Une toute nouvelle page s'est ouverte dans laquelle j'ai pu constater que je n'avais encore aucun **outil**. En effet, il faut d'abord que j'encode sur le programme les informations de l'outil en question.   
    
  Pour cela, j'ai cliqué sur le bouton **"+"** en haut à gauche ce qui m'a ouvert, à nouveau, une page dans laquelle je devais choisir le type de fraise que j'allais utiliser, dans mon cas, une **fraise deux tailles**.  
  Une fois sélectionné, une nouvelle page s'ouvre dans laquelle je peux voir, sur la droite, un cylindre gris et orange.  
  Sur la gauche, je vois ceci :  
    
![](images/fusion360-outil1.png)
    
  Là, je peux inscrire dans **distributeur** le lien d'achats de ma fraise pour facilement la racheter, je peux décrire mon outil dans **description** mais rien de cela n'est obligatoire.  
  Ce que j'ai dû faire obligatoirement s'est que j'ai dû cliquer sur **Coupant** en haut à gauche et encoder les données correspondant à la fraise que je voulais utiliser.  
  J'ai simplement regardé les informations concernant la fraise sur l'encyclopédie des fraises, là, il y a toutes les informations que je dois encoder dans le programme.  
    
![](images/fusion360-actions5.png)
      
  Pour finir, j'ai cliqué sur **Données de coupe** et j'y ai encodé la **Vitesse de broche** correspondant 25000 tr/min pour ma fraise.  
  J'ai cliqué sur *Accepter* et ainsi créé l'outil avec lequel j'allais travailler.  
    
  Une fois mon outil sélectionné, j'ai cliqué sur le petit bouton *"géométrie"* dans le haut du petit onglet *adaptatif 2D* et j'ai cliqué sur la surface de l'objet sur laquelle je voulais effectuer l'action. Une fois sélectionnée, elle devient bleue.  
  J'ai, ensuite, fais des modifications dans *"passes"*. J'ai supprimé l'option **surépaisseur** et ajouté l'option **profondeurs multiples**. Là, j'ai dû changer la donnée de "*profondeur de passe maximum d'ébauche* afin d'y indiquer **4 mm** car il s'agit de la moitié du diamètre de ma fraise (qui est de 8 mm dans mon cas).  
  Enfin, je clique sur le bouton *liaison* et je vérifie que mon type de rampe est bien **l'hélice**.  
    
  Quand tout ceci était fait, j'ai cliqué sur **OK** et j'ai vu ceci sur mon objet :  
    
![](images/fusion360-actions6.png) 
    
  Cela montre comment la machine s'y prendra pour fraiser la surface.
    
* **Contour** : L'option de contour est ce qui va dire à la CNC de découper votre pièce de votre brut. Il s'agit d'une étape importante qui se configure de cette manière :  
  
1) Cliquer sur **2D**  
2) Cliquer sur **Contour 2D**  
3) Dans le nouvel onglet, aller sélectionner l'outil.  
  
**_Attention_** : il est important de choisir le même outil que celui choisis pour le reste de vos actions pour éviter de devoir changer d'outil en cours de fraisage.  
  
4) Cliquer sur le bouton *géométrie* et sélectionner le contour du **dessous** de la pièce.  
5) Cliquer sur le petit carré devant l'option **onglet**.  
Les onglets sont des parties du contour qui ne vont pas être fraisés. Cela permet à l'objet de ne pas bouger tout au long du fraisage.  
6) Choisir dans *positionnement d'onglets* le mode *nombre d'ongets*.  
7) Choisir le nombre d'onglets que l'on veut et modifier, si on veut, les mesures de ceux-ci. Ces onglets seront à enlever manuellement une fois le fraisage terminé.  
8) Cliquer sur le bouton **hauteur** et modifier dans *hauteur sur le fond* la mesure pour le décalage. Moi j'ai mis -0.5. Cela permet d'être sûr que la fraise va fraiser assez profond pour que le brut se détache de l'objet.  
**_Attention_** ne pas mettre un décalage trop grand pour éviter que la machine fraise trop dans la planche sur laquelle notre brut est déposé.  
9) Aller dans **passes** et ajouter l'option *profondeurs multiples* dans laquelle je modifie la *profondeur de passe maximum d'ébauche* que je mets à **4 mm**.  
10) Aller dans **liaisons**, désélectionner *entrée* et *sortie* et ajouter une rampe dont je mets l'angle de rampe à **4 deg**.  
11) Appuyer sur **OK**.  
  
* Il existe encore de nombreuses choses possibles à faire sur le programme Fusion360, mais ces deux actions sont les plus importantes et celles que j'ai dû utiliser plus tard.  
  
#### 4.2.1.6 **Enregistrer**
  
Une fois que j'ai terminé de configurer toutes mes fabrications, je peux simuler le fraisage en cliquant en haut à droite sur **actions** puis sur **simuler**. Une fois que j'en suis satisfaite, je clique à nouveau sur **actions** et ensuite je clique sur **post-traiter**.  
  
![](images/fusion360-actions7.png)  
  
J'arrive à une nouvelle page sur laquelle je peux modifier le **nom du fichier**.  
  
![](images/fusion360-actions8.png)  
  
En cliquant sur le petit dossier à droite de **Dossier de sortie**, je vais pouvoir choisir où enregistrer mon travail.  

![](images/fusion360-actions9.png)  
  
Une fois le dossier sélectionné, je clique sur **post-processeur**. Dès lors, mon travail est enregistré en tant que document *".cn"*. Pour passer à l'étape *machine*, il faut que je mette mon document dans une clé USB.  

### 4.2.2 Apprendre à utiliser la fraiseuse CNC
  
Je ne vais rien écrire dans ce point car dans la partie **4.3 Cours 2** au point **4.3.3**, je vais décrire chaque étape à faire pour démarrer la machine après avoir fait quelques encodages sur le programme **KinetiC NC**. Pour éviter de me répéter, je ne décrirais rien dans ce point-ci même si lors du cours 1, les formateurs nous ont montré comment utiliser la fraiseuse.  
  
## 4.3 Cours 2 : Deuxième partie de la formation

### 4.3.1 Conception de mon objet sur Fusion360
  
Pour la conception de mon objet, j'ai dû regarder une [vidéo youtube](https://youtu.be/ExHGnBdvz7o) pour comprendre comment faire.  
Grâce à celle-ci, j'ai pu concevoir une décoration pour ma chambre. Simple, à fraisage rapide et en seulement quelques clics.  
  
![](images/fusion360-conception1.png)  
  
En seulement 2 esquisses et 2 extrusions, j'ai conçu ce soleil caché derrière un nuage.  

![](images/fusion360-conception2.png)  
 
### 4.3.2 Mode "Fabriquer"
  
Après la conception, je suis passée en mode **Fabriquer**.  
J'ai commencé par effectuer la configuration de mes axes comme je l'ai expliqué dans le point **4.2.1.3** et ensuite configuré mon brut.  
Je n'ai dû faire qu'un usinage adaptatif 2D et un contour comme actions pour que la machine fraise ma création.  
  
J'ai configuré l'usinage adaptatif 2D en commençant par choisir mon outil. J'ai ensuite sélectionné la surface supérieure du soleil et dans **passes**, j'ai supprimé la surépaisseur et ajouté les profondeurs multiples dans lesquelles j'ai modifié la *profondeur de passe maximaum d'ébauche*. Enfin, j'ai vérifié dans **liaison** que mon type de rampe était bien *hélice* et j'ai cliqué sur **OK**. Tous ceci est expliqué au point **4.2.1.4** 
  
![](images/fusion360-conception3.png)  
  
Pour le contour, j'ai choisi le même outil que pour l'usinage, j'ai choisi le contour qui reprenait le nuage et le soleil, donc celui du dessous de mon objet et j'ai ajouté quatres onglets. J'ai ensuite modifié, dans **hauteur**, le décalage dans **hauteur sur le fond**, j'ai mis **-0.5 mm**, puis je suis allée ajouter les *profondeurs multiples* dans lesquelles j'ai encodé **4 mm** pour la *profondeur de passe maximum d'ébauche*. Enfin, dans **liaison**, j'ai supprimé les options **entrée** et **sortie** et j'ai ajouté une rampe dont l'angle vaut **4 degré**.  
J'ai cliqué sur **OK** et il ne me restait plus que l'enregistrement sur clé USB à faire.  
  
![](images/fusion360-conception4.png)  
  
### 4.3.3 Préparer au fraisage
  
La fraiseuse :  
![](images/image16.jpg)  
  
#### 4.3.3.1 Préparer mon brut au fraisage
  
Mon brut est une simple planche de bois dont j'ai noté les mesures précises lors de la configuration de celui-ci dans le programme Fusion360. Cette planche est celle dans laquelle la fraiseuse va fraiser. Avant de la mettre dans la machine, je dois passer par quelques étapes importantes.  
1) Prendre plusieurs autres planches à déposer en dessous de mon brut afin d'être sûre de ne pas abimer la surface de la fraiseuse CNC. En d'autres mot, j'ajoute de la matière en dessous pour que, lors de mon fraisage, rien ne perçe dans la surface de la machine. L'objectif étant d'éviter d'abimer la machine.  
2) Percer, à l'aide d'une perceuse, quatres trous au quatres coins de mon brut. Les trous doivent passer au travers du brut **et** des planches supplémentaires placées en dessous de celui-ci ! Plus tard, des vis seront placées dans ces trous pour fixer le brut à la surface de la machine pour éviter qu'il ne bouge lors du fraisage.  
![](images/image1-test.jpg)  
3) Dessiner au crayon sur mon brut le point (0;0;0)  
![](images/image3.jpg)
  
#### 4.3.3.2 Programmer le fraisage sur KinetiC-NC
  
Une fois mon brut prêt, j'ai dû entrer la clé USB dans l'ordinateur lié à la fraiseuse CNC afin de récupérer le fichier enregistré depuis Fusion360.  
Avant d'utiliser le progamme KinetiC-NC pour configurer mon point à l'origine, mon point (0;0;0), j'ai dû fixer mon brut à la surface de la CNC.   
  
![](images/image2.jpg)  
  
J'ai également dû choisir la bonne vitesse de fraisage. Pour cela, il faut tourner une roulotte qui se trouve sur la partie de la machine où se place la fraise, au dessus sur la gauche.  
  
![](images/image16-2.jpg)  
  
**Programmation de la machine :**  
  
1) J'ai ouvert le programme et ensuite cliqué sur **Jog/Setup** (1). J'ai ensuite cliqué sur **to zero point** (2) ce qui a ouvert un nouvel onglet sur lequel j'ai cliqué sur **Home Now** (3).  
![](images/image8-2.jpg)  
2) A l'aide des flèches du clavier, j'ai déplacé ma fraise de façon à la placer au dessus de la marque au crayon indiquant mon point (0;0;0) sur le brut.
3) Ensuite, sur l'écran, en haut à droite, j'ai dû cliquer sur les boutons oranges **0** devant X et Y pour indiquer à la machine que là où se trouve la fraise est là où X et Y valent 0.  
![](images/image6.jpg)  
4) Pour le point d'origine de Z, c'est un peu plus compliqué. J'ai dû placer un mobile métallique, dont les mesures sont déjà encodée dans la machine, en dessous de la fraise.  
![](images/image4.jpg)  
Une fois bien placé, sur mon programme, j'ai cliqué sur **Custom** et ensuite j'ai dû **double-cliquer** sur **Z0-finder with mobile tool leng...**.  
![](images/axe-z-mobile1.jpg)  
Je vais alors, avec le clavier, faire descendre ma fraise jusqu'à ce qu'elle frôle le dessus du mobile. La machine connaissant les mesures du mobile saura dès lors où se trouve le 0 en Z.  
5) J'ai ensuite ouvert mon fichier en cliquant sur **file** en haut à gauche de l'écran et ensuite sur **Open NC-File**.  
Le bureau s'est ouvert et j'ai sélectionné le bon fichier.  
6) Une fois le fichier ouvert, je vois apparaître sur le programme une image de mon objet.  
![](images/image11.jpg)  

7) Il ne me reste plus qu'à lancer le démarrage du fraisage en cliquant sur le bouton orange **play**, en bas à gauche sur l'image ci-dessus.  

  Après tout ceci, la fraiseuse va démarrer et commencer à fraiser dans le brut.   
    
#### 4.3.3.3 A quoi faire attention et quelques astuces  
  
* Lorsqu'on utilise une fraiseuse, il ne faut pas oublier d'activer *l'aspirateur* lié à celle-ci. L'aspirateur aspirera tous les miniscules copaux de bois pour éviter que ceux-ci ne brûlent.  
* Avant de démarrer la machine, il faut vérifier que notre *fraise est bien serrée*. 
* Il faut faire attention avec les portes de la machine car celles-ci sont *fortement aimantées*.
* Il faut porter un casque sur les oreilles car la machine fait beaucoup de bruit. 
* Il faut toujours rester à côté de la machine lorsqu'elle est lancée. J'ai personnellement senti une odeur de brûlé lors de mon fraisage. J'ai arrêté la machine en cliquant sur le bouton **stop** sur le programe KinetiC-NC, ouvert les portes et ai vérifié que l'aspirateur fonctionnait bien, que la fraise était bien serrée et que la vitesse était la bonne. J'ai légèrement baissé la vitesse et redémarré le fraisage en appuyant à nouveau sur le bouton **play**
* Sur la droite d'un clavier d'ordinateur, les chiffres 2, 4, 6 et 8 servent également de flèches pour *déplacer la fraise*.  
  
### 4.3.4 résultat

[Ici](https://youtu.be/JT32kXkKC20), vous trouverez une vidéo montrant à quoi ressemblait le fraisage de ma décoration.  
  
Une fois le fraisage terminé, avant de dévisser notre brut, il est intérressant de vérifier que le contour ai été bien fait. Il suffit, avec n'importe quel outil, de soulever le brut pour vérifier qu'il soit bien détaché de notre objet final.  
     
![](images/image17.jpg)   
  
Si c'est le cas, je peux dévisser mes planches de la surface et la sortir de la machine.  
  
**_Attention_** : Afin de laisser une machine en bon état pour le suivant, je n'oublie pas de passer l'aspirateur dans la machine !  
  
Lorsque j'ai retiré mes planches, je peux voir que le contour a été bien effectué et qu'il ne reste plus que les onglets que je dois retirer moi-même.  

![](images/image19.jpg)  
  
Une fois tous mes onglets enlevés, mon objet final est totalement détaché de ma planche initiale. Je l'ai poncé et voici mon résultat final :  

![](images/image21.jpg)  
  
J'ai ainsi fraisé une décoration pour ma chambre que je peindrais plus tard. 