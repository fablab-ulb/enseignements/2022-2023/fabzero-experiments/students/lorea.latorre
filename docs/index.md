## A propos de moi

![Test](images/Photo_Camp_Route.JPG)

Salut! Je m'appelle Lorea Latorre. J'ai été en grande galère le premier jour où j'ai dû écrire ce *à propos* mais maintenant je ne dirais pas que je suis une pro, mais je me débrouille un peu mieux qu'au début !  
  
Au début, je ne savais même pas comment vérifier si ma photo était visible... pas fifou? On est d'accord mais promis je me suis améliorée ! Je t'invites donc à lire la suite de ce blog pour en découvrir plus !

## MAJ 
  
Mon site ce mets à jour durant les nuits de lundi à mardi ainsi que les nuits du mercredi à jeudi. Simplement car il s'agit des MAJ automatiques décidées par le professeur.   
  
## A quoi ressemble ma vie ?

Je suis née à Bruxelles et y ai vécu toute ma vie. Je ne compte pas y passer mon futur mais pour l'instant je n'y pense pas trop, je laisserai mon **_futur moi_** prendre la décision.  
Au jour d'aujourd'hui, ma vie ce résume à mes études et au scoutisme. Je suis entre ma première et ma deuxième année de bachelier en biologie (après quelques années désastreuses dont je ne parlerai pas) et entame ma deuxième année en tant que cheffe scout. Le scoutisme s'est vraiment un point important de ma vie présente. Il me prend beaucoup de temps et de forces mais je n'imagine pas ma vie sans lui.  
Pourquoi je parle de scoutisme ici? Je me suis posée la question et la réponse est simple, c'est parce que je ne sais pas quoi vous raconter et qu'il s'agit d'un sujet qui me fait vivre donc pourquoi pas après tout?
Pour les curieux, mon totem est Altaïca, une belette de montagne !  
  
**Unité 33-81 <3**

## Anciens travaux 
  
N'étant qu'entre ma première et deuxième année de bachelier, je n'ai encore jamais eu de progès/travaux à accomplir. J'estime que ce cours sera mon premier travail dans mes études, et même si cela n'a pas grand chose à voir avec la biologie (= mes études), je suis contente que cela se passe comme ça !